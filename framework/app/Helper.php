<?php

if (!function_exists('get_setting')) {
    
    function get_setting($field_name){

        $setting = new App\Setting();

        return $setting->get_setting($field_name);
    }
}

?>