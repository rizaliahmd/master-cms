<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    public function get_setting($field_name){
    	$setting = Setting::where("field_name", $field_name)->firstOrFail();

    	return $setting->field_value;
    }
}
