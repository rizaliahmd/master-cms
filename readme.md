<p><h1 align="center">MASTER CMS</h1></p>

## About 

- [Laravel](https://laravel.com).
- [Admin LTE 2](https://adminlte.io/themes/AdminLTE/index2.html).


## How to Install

- From terminal, go to framework folder and type `composer install`
- copy `framework/.env.example` to `framework/.env`
- create DATABASE
- Edit file .env, Set database configuration
- from terminal inside framework folder type `php artisan migrate`